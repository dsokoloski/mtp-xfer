// MTP-xfer
// Copyright (C) 2016 Darryl Sokoloski <darryl@sokoloski.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <map>
#include <string>
#include <sstream>

#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <stdbool.h>
#include <grp.h>
#include <errno.h>

#include <libmtp.h>

#include "INIReader.h"

#define _MTPX_CONFIG        "/usr/local/etc/mtp-xfer.conf"
#define _MTPX_BASE_PATH     "/tmp"
#define _MTPX_DIR_MODE      0750
#define _MTPX_FILE_MODE     0640
#define _MTPX_GROUP_NAME    "media"

using namespace std;

typedef map<uint32_t, pair<bool, string> > id_filename_map_t;

typedef struct
{
    string base_path;
    string dest_path;
    mode_t dir_mode;
    mode_t file_mode;
    gid_t group_id;
    bool delete_files;
} mtpx_config_t;

static mtpx_config_t mtpx_config;

static void mtpx_usage(int rc = 0, bool version = false)
{
    fprintf(stdout, "%s v%s [libmtp v%s]\n",
        PACKAGE_NAME, PACKAGE_VERSION, LIBMTP_VERSION_STRING);
    fprintf(stdout,
        "Copyright (C) 2016 Darryl Sokoloski <darryl@sokoloski.ca>\n");
#ifdef PACKAGE_BUGREPORT
    fprintf(stdout, "Report bugs to: %s\n", PACKAGE_BUGREPORT);
#endif
    if (version) {
        fprintf(stdout,
            "  This program comes with ABSOLUTELY NO WARRANTY.\n");
        fprintf(stdout,
            "  This is free software, and you are welcome to redistribute it\n");
        fprintf(stdout,
            "  under certain conditions according to the GNU General Public\n");
        fprintf(stdout,
            "  License version 3, or (at your option) any later version.\n");
    }
    else {
        fprintf(stdout,
            "  -V, --version\n");
        fprintf(stdout,
            "    Display program version and license information.\n");
        fprintf(stdout,
            "  -c, --config <filename>\n");
        fprintf(stdout,
            "    Configuration file.  Default: %s\n", _MTPX_CONFIG);
        fprintf(stdout,
            "  -D, --delete-files\n");
        fprintf(stdout,
            "    Delete files from device after a successful transfer.\n");
    }

    exit(rc);
}

static int mtpx_load_config(const char *path)
{
    struct stat st;
    if (stat(path, &st) < 0) return 1;

    INIReader reader(path);

    if (reader.ParseError() != 0) {
        fprintf(stderr, "Unable to parse configuration file: %s\n",
            path);
        return 1;
    }

    mtpx_config.base_path = reader.Get(
        "mtp-xfer", "base-path", _MTPX_BASE_PATH);

    string group_name = reader.Get(
        "mtp-xfer", "group-name", _MTPX_GROUP_NAME);

    struct group *gr = getgrnam(_MTPX_GROUP_NAME);
    if (gr == NULL) {
        fprintf(stderr, "Unable to resolve group name: %s\n",
            group_name.c_str());
        return 1;
    }

    mtpx_config.group_id = gr->gr_gid;
}

static int mtpx_mkdir_p(const char *path)
{
    struct stat st;

    if (lstat(path, &st) != 0) {
        if (errno == ENOENT) {
            if (mkdir(path, mtpx_config.dir_mode) != 0) {
                fprintf(stderr, "Unable to create directory: %s: %s\n",
                    path, strerror(errno));
                return 1;
            }

            if (chown(path, geteuid(), mtpx_config.group_id) != 0) {
                fprintf(stderr, "Unable to change group owner: %s: %s\n",
                    path, strerror(errno));
                return 1;
            }

            fprintf(stdout, "Created directory: %s\n", path);
        }
        else {
            fprintf(stderr, "Unable to stat path: %s: %s\n",
                path, strerror(errno));
            return 1;
        }
    }

    return 0;
}

static int mtpx_set_path(const char *serial)
{
    if (mtpx_mkdir_p(mtpx_config.base_path.c_str()) != 0) return 1;

    ostringstream os;
    os << mtpx_config.base_path << "/" << serial;

    if (mtpx_mkdir_p(os.str().c_str()) != 0) return 1;

// %F: YYYY-MM-DD (10 + NULL)
#define __MTPX_DATE_SIZE    (10 + 1)
    time_t now;
    char date_stamp[__MTPX_DATE_SIZE];

    time(&now);
    strftime(date_stamp, __MTPX_DATE_SIZE, "%F", localtime(&now));

    os << "/" << date_stamp;

    if (mtpx_mkdir_p(os.str().c_str()) != 0) return 1;

    mtpx_config.dest_path = os.str().c_str();

    return 0;
}

static int mtpx_progress(
    const uint64_t sent, const uint64_t total, void const * const data)
{
    const char *filename = (const char *)data;

    if (sent == total)
        fprintf(stdout, "%s: %lu of %lu bytes transferred.\n", filename, sent, total);
    else {
        int percent = (sent * 100) / total;
        fprintf(stdout, "%s: %lu of %lu bytes (%3d%%)\r", filename, sent, total, percent);
        fflush(stdout);
    }

    return 0;
}

void mtpx_file_tree(
    LIBMTP_mtpdevice_t *device,
    LIBMTP_devicestorage_t *storage, uint32_t leaf,
    id_filename_map_t &id_filename_map)
{
    LIBMTP_file_t *file, *files;

    files = LIBMTP_Get_Files_And_Folders(device, storage->id, leaf);
    if (files == NULL) return;

    file = files;
    while (file != NULL) {
        int i;
        LIBMTP_file_t *oldfile;

        switch (file->filetype) {
        case LIBMTP_FILETYPE_MP4:
        case LIBMTP_FILETYPE_WMV:
        case LIBMTP_FILETYPE_AVI:
        case LIBMTP_FILETYPE_MPEG:
        case LIBMTP_FILETYPE_ASF:
        case LIBMTP_FILETYPE_QT:
        case LIBMTP_FILETYPE_UNDEF_VIDEO:
        case LIBMTP_FILETYPE_JPEG:
            fprintf(stdout, " %8u: %s\n", file->item_id, file->filename);
            id_filename_map[file->item_id] = make_pair(false, file->filename);
            break;

        case LIBMTP_FILETYPE_FOLDER:
            mtpx_file_tree(device, storage,
                file->item_id, id_filename_map);
            break;

        case LIBMTP_FILETYPE_UNKNOWN:
            if (strlen(file->filename) > 4) {
                int len = strlen(file->filename);
                char *ext = &file->filename[len - 4];
                if (ext[0] != '.') break;
                if (strncasecmp(ext, ".jpg", 4) && strncasecmp(ext, ".mp4", 4)) break;
                fprintf(stdout, " %8u: %s [by extension]\n", file->item_id, file->filename);
                id_filename_map[file->item_id] = make_pair(false, file->filename);
            }
            break;

//        default:
//            fprintf(stdout, " %8u: %s [%d]\n", file->item_id, file->filename,
//                file->filetype);
        }

        oldfile = file;
        file = file->next;
        LIBMTP_destroy_file_t(oldfile);
    }
}

static int mtpx_file_xfer(LIBMTP_mtpdevice_t *device, uint32_t id, const char *filename)
{
    struct stat st;
    stringstream os;

    os << mtpx_config.dest_path << "/" << filename;

    if (lstat(os.str().c_str(), &st) == 0) {
        fprintf(stderr, "File already exists: %s\n", filename);
        return 1;
    }

    if (LIBMTP_Get_File_To_File(device, id,
        os.str().c_str(), mtpx_progress, filename) != 0 ) {
        fprintf(stderr, "\nError getting file from device.\n");
        LIBMTP_Dump_Errorstack(device);
        LIBMTP_Clear_Errorstack(device);
        return 1;
    }

    if (chown(os.str().c_str(), geteuid(), mtpx_config.group_id) != 0) {
        fprintf(stderr, "Unable to change file ownership: %s: %s\n",
            os.str().c_str(), strerror(errno));
    }

    if (chmod(os.str().c_str(), mtpx_config.file_mode) != 0) {
        fprintf(stderr, "Unable to change file permissions: %s: %s\n",
            os.str().c_str(), strerror(errno));
    }

    return 0;
}

int main(int argc, char *argv[])
{
    const char *config = NULL;
    LIBMTP_error_number_t en;
    int rc, raw_device_count = 0;
    LIBMTP_raw_device_t *raw_devices;
    id_filename_map_t id_filename_map;

    mtpx_config.base_path = _MTPX_BASE_PATH;
    mtpx_config.dir_mode = _MTPX_DIR_MODE;
    mtpx_config.file_mode = _MTPX_FILE_MODE;
    mtpx_config.delete_files = false;

    static struct option options[] =
    {
        { "help", 0, 0, 'h' },
        { "version", 0, 0, 'V' },
        { "config", 1, 0, 'c' },
        { "delete-files", 0, 0, 'D' },

        { NULL, 0, 0, 0 }
    };

    for (optind = 1;; ) {
        int o = 0;
        if ((rc = getopt_long(argc, argv,
            "?hVc:D", options, &o)) == -1) break;
        switch (rc) {
        case '?':
            fprintf(stdout,
                "Try %s --help for more information.\n", argv[0]);
            return 1;
        case 'h':
            mtpx_usage();
        case 'V':
            mtpx_usage(0, true);
        case 'c':
            config = optarg;
            break;
        case 'D':
            mtpx_config.delete_files = true;
            break;
        default:
            mtpx_usage(1);
        }
    }

    fprintf(stdout, "%s v%s [libmtp v%s]\n",
        PACKAGE_NAME, PACKAGE_VERSION, LIBMTP_VERSION_STRING);

    if (config != NULL && mtpx_load_config(config) != 0) {
        fprintf(stderr, "Unable to load configuration: %s\n", config);
        return 1;
    }
    else
        mtpx_load_config(_MTPX_CONFIG);

    LIBMTP_Init();

    en = LIBMTP_Detect_Raw_Devices(&raw_devices, &raw_device_count);

    switch (en) {
    case LIBMTP_ERROR_NO_DEVICE_ATTACHED:
        fprintf(stdout, "No raw devices found.\n");
        return 0;
    case LIBMTP_ERROR_CONNECTING:
        fprintf(stderr, "Error connecting to device.\n");
        return 1;
    case LIBMTP_ERROR_MEMORY_ALLOCATION:
        fprintf(stderr, "Memory allocation error.\n");
        return 1;
    case LIBMTP_ERROR_NONE:
        fprintf(stderr, "Detected %d device(s).\n", raw_device_count);
        break;
    case LIBMTP_ERROR_GENERAL:
    default:
        fprintf(stderr, "Unknown connection error.\n");
        return 1;
    }

    fprintf(stdout, "Destination path: %s\n", mtpx_config.base_path.c_str());

    for (int d = 0; d < raw_device_count; d++) {
        char *name, *serial;
        unsigned long n, s = 0;
        LIBMTP_mtpdevice_t *device;
        LIBMTP_devicestorage_t *storage;
        id_filename_map_t::iterator i;

        device = LIBMTP_Open_Raw_Device_Uncached(&raw_devices[d]);
        if (device == NULL) {
            fprintf(stderr, "Unable to open raw device #%d.\n", d);
            continue;
        }

        LIBMTP_Dump_Errorstack(device);
        LIBMTP_Clear_Errorstack(device);

        name = LIBMTP_Get_Friendlyname(device);
        serial = LIBMTP_Get_Serialnumber(device);

        fprintf(stdout, "\nConnected to device #%d of %d",
            d + 1, raw_device_count);
        if (name != NULL) {
            fprintf(stdout, ": %s", name);
            free(name);
        }
        if (serial != NULL) {
            if (name != NULL) fprintf(stdout, ", ");
            fprintf(stdout, ": %s", serial);
        }
        else
            serial = strdup("00000000deadbeef");

        if (name == NULL && serial == NULL) fputc('.', stdout);
        fputc('\n', stdout);

        if (mtpx_set_path(serial) != 0)
            goto __mtpx_next_device;

        if (LIBMTP_Get_Storage(device, LIBMTP_STORAGE_SORTBY_NOTSORTED))
            fprintf(stderr, "Error getting storage entry.\n");
        else {
            for (storage = device->storage;
                storage != 0; storage = storage->next) {

                int percent = 0;

                fprintf(stdout, "Storage: %s\n",
                    storage->StorageDescription);

                id_filename_map.clear();
                mtpx_file_tree(device, storage, 0, id_filename_map);

                if (id_filename_map.size() == 0) {
                    fprintf(stdout, "No files found to transfer.\n");
                    continue;
                }

                n = 0;
                for (i = id_filename_map.begin();
                    i != id_filename_map.end(); i++) {

                    percent = (++n * 100) / (
                        (mtpx_config.delete_files) ?
                            (id_filename_map.size() * 2) : id_filename_map.size()
                    );
                    fprintf(stdout, "Transferring file #%lu of %lu (%3d%%): %s\n",
                        n, id_filename_map.size(), percent, i->second.second.c_str());

                    if (mtpx_file_xfer(device, i->first, i->second.second.c_str()))
                        continue;

                    i->second.first = true;
                }

                if (mtpx_config.delete_files == false) continue;

                n = 0;
                for (i = id_filename_map.begin();
                    i != id_filename_map.end(); i++) {

                    percent = (((++n) + id_filename_map.size()) * 100) / (id_filename_map.size() * 2);
                    fprintf(stdout, "%seleting file #%lu of %lu (%3d%%): %s\n",
                        (i->second.first) ? "D" : "Not d",
                        n, id_filename_map.size(), percent, i->second.second.c_str());

                    if (!i->second.first) continue;

                    if (LIBMTP_Delete_Object(device, i->first) != 0) {
                        LIBMTP_Dump_Errorstack(device);
                        LIBMTP_Clear_Errorstack(device);
                    }
                }
            }
        }

__mtpx_next_device:
        free(serial);
        LIBMTP_Release_Device(device);
    }

    free(raw_devices);

    return 0;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
