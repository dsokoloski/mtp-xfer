MTP-xfer
========

MTP-xfer transfers media files (images and videos), specifically:
 - ASF
 - AVI
 - JPEG
 - MOV (QT)
 - MP4
 - MPEG
 - WMV

These files are copied to the configured path and optionally deleted
from the MTP device.

Configuration
-------------

TODO

Compilation Notes
-----------------

Ensure the repository is cloned using the --recursive option.
